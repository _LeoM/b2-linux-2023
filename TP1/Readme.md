TP1 : Premiers pas Docker

I. Init

3. sudo c pa bo

🌞 Ajouter votre utilisateur au groupe docker

```bash
[lm33@localhost ~]$ groups lm33
lm33 : lm33 wheel
[lm33@localhost ~]$ sudo usermod -aG docker lm33
[lm33@localhost ~]$ groups lm33
lm33 : lm33 wheel docker
[lm33@localhost ~]$ exit
logout


Last login: Thu Dec 21 15:05:15 2023 from 10.1.1.1
[lm33@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

4. Un premier conteneur en vif

🌞 Lancer un conteneur NGINX

```bash
[lm33@localhost ~]$ docker run -d -p 9999:80 nginx
...
7eed5b874ea6d7527c4d00ab73f441a43d4690613acc96b635d3feeb49f61760
[lm33@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS                                   NAMES
7eed5b874ea6   nginx     "/docker-entrypoint.…"   About a minute ago   Up About a minute   0.0.0.0:9999->80/tcp, :::9999->80/tcp   inspiring_chatterjee
```

🌞 Visitons

```bash
[lm33@localhost ~]$ docker logs inspiring_chatterjee
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/21 14:33:27 [notice] 1#1: using the "epoll" event method
2023/12/21 14:33:27 [notice] 1#1: nginx/1.25.3
2023/12/21 14:33:27 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
2023/12/21 14:33:27 [notice] 1#1: OS: Linux 5.14.0-70.26.1.el9_0.x86_64
2023/12/21 14:33:27 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1073741816:1073741816
2023/12/21 14:33:27 [notice] 1#1: start worker processes
2023/12/21 14:33:27 [notice] 1#1: start worker process 28
[lm33@localhost ~]$ docker inspect inspiring_chatterjee
[
    {
        "Id": "7eed5b874ea6d7527c4d00ab73f441a43d4690613acc96b635d3feeb49f61760",
        "Created": "2023-12-21T14:33:27.010065685Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 49085,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-21T14:33:27.423399402Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:d453dd892d9357f3559b967478ae9cbc417b52de66b53142f6c16c8a275486b9",
        ...
]
[lm33@localhost ~]$ sudo ss -lnpt | grep docker
LISTEN 0      4096         0.0.0.0:9999      0.0.0.0:*    users:(("docker-proxy",pid=49043,fd=4))
LISTEN 0      4096            [::]:9999         [::]:*    users:(("docker-proxy",pid=49048,fd=4))
[lm33@localhost ~]$ sudo firewall-cmd --add-port 9999/tcp --permanent
success
[lm33@localhost ~]$ sudo firewall-cmd --reload
success
[lm33@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 9999/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 On va ajouter un site Web au conteneur NGINX | 🌞 Visitons

```bash
[lm33@localhost ~]$ mkdir nginx
[lm33@localhost ~]$ echo "<h1>MEOOW</h1>" > ./nginx/index.html
[lm33@localhost ~]$ cat nginx/index.html
<h1>MEOOW</h1>
[lm33@localhost ~]$ echo "server {
    listen        8080;

    location / {
        root /var/www/html;
    }
}" > ./nginx/site_nul.conf
[lm33@localhost ~]$ cat nginx/site_nul.conf
server {
    listen        8080;

    location / {
        root /var/www/html;
    }
}
[lm33@localhost ~]$ docker run -d -p 9999:8080 -v /home/lm33/nginx/index.html:/var/www/html/index.html
 -v /home/lm33/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
454ec0456e515fca50e903a7a5d26272722cd5b0500e9d23749a9cd177e49aca
[lm33@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS
                                 NAMES
454ec0456e51   nginx     "/docker-entrypoint.…"   26 seconds ago   Up 26 seconds   80/tcp, 0.0.0.0:9999->8080/tcp, :::9999->8080/tcp   strange_khayyam
```

5. Un deuxième conteneur en vif

🌞 Lance un conteneur Python, avec un shell

```bash
[lm33@localhost ~]$ docker run -it python bash
...
root@929683b1840c:/# python -v
...
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

🌞 Installe des libs Python

```bash
root@929683b1840c:/# pip install aiohttp
Collecting aiohttp
...
Successfully installed aiohttp-3.9.1 aiosignal-1.3.1 attrs-23.1.0 frozenlist-1.4.1 idna-3.6 multidict-6.0.4 yarl-1.9.4
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
root@929683b1840c:/# pip install aioconsole
Collecting aioconsole
...
Successfully installed aioconsole-0.7.0
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
root@929683b1840c:/# python
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import aiohttp
>>>
```

II. Images

1. Images publiques

🌞 Récupérez des images

```bash
[lm33@localhost ~]$ docker pull python:3.11
3.11: Pulling from library/python
...
Digest: sha256:4e5e9b05dda9cf699084f20bb1d3463234446387fa0f7a45d90689c48e204c83
Status: Downloaded newer image for python:3.11
docker.io/library/python:3.11
[lm33@localhost ~]$ docker pull mysql:5.7-debian
5.7-debian: Pulling from library/mysql
...
Digest: sha256:0821f3a5b3ecda79885d8bd40ec353f3d2cc1bc23586f9c367dfc76493737163
Status: Downloaded newer image for mysql:5.7-debian
docker.io/library/mysql:5.7-debian
[lm33@localhost ~]$ docker pull wordpress
Using default tag: latest
latest: Pulling from library/wordpress
...
Digest: sha256:be7173998a8fa131b132cbf69d3ea0239ff62be006f1ec11895758cf7b1acd9e
Status: Downloaded newer image for wordpress:latest
docker.io/library/wordpress:latest
[lm33@localhost ~]$ docker pull linuxserver/wikijs
Using default tag: latest
latest: Pulling from linuxserver/wikijs
...
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest
[lm33@localhost ~]$ docker images
REPOSITORY           TAG          IMAGE ID       CREATED        SIZE
linuxserver/wikijs   latest       869729f6d3c5   6 days ago     441MB
python               latest       fc7a60e86bae   13 days ago    1.02GB
wordpress            latest       fd2f5a0c6fba   2 weeks ago    739MB
python               3.11         22140cbb3b0c   2 weeks ago    1.01GB
nginx                latest       d453dd892d93   8 weeks ago    187MB
mysql                5.7-debian   6dca13361869   4 months ago   463MB
```

🌞 Lancez un conteneur à partir de l'image Python

```bash
[lm33@localhost ~]$ docker run -it 22140cbb3b0c bash
root@3c62ef7fe7de:/# python --version
Python 3.11.7
```

2. Construire une image

🌞 Ecrire un Dockerfile pour une image qui héberge une application Python

```bash
[lm33@localhost python_app_build]$ ls -al
total 8
drwxr-xr-x. 2 lm33 lm33  38 Dec 21 16:38 .
drwx------. 4 lm33 lm33 152 Dec 21 16:38 ..
-rw-r--r--. 1 lm33 lm33  97 Dec 21 16:38 app.py
-rw-r--r--. 1 lm33 lm33 178 Dec 21 16:36 Dockerfile
[lm33@localhost python_app_build]$ cat Dockerfile
FROM debian

RUN apt update -y && apt install -y python3-pip

RUN python3 -m pip install emoji --break-system-packages

RUN mkdir /app

WORKDIR /app

COPY app.py /app/app.py

ENTRYPOINT ["python3", "app.py"]
[lm33@localhost python_app_build]$ cat app.py
import emoji

print(emoji.emojize("Cet exemple d'application est vraiment naze :thumbs_down:"))

```

🌞 Build l'image


```bash
[lm33@localhost python_app_build]$ docker build . -t python_app:version_de_ouf
[+] Building 0.7s (11/11) FINISHED                                                                       docker:default
 => [internal] load build definition from Dockerfile                                                               0.1s
...
 => => naming to docker.io/library/python_app:version_de_ouf
 [lm33@localhost python_app_build]$ docker images | grep python_app
python_app           version_de_ouf   1c3bd8ccb877   3 minutes ago   636MB
```

🌞 Lancer l'image

```bash
[lm33@localhost python_app_build]$ docker run python_app:version_de_ouf
Cet exemple d'application est vraiment naze 👎
```

III. Docker compose

🌞 Créez un fichier docker-compose.yml

```bash
[lm33@localhost compose_test]$ echo "version: '3'

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999" > docker-compose.yml
[lm33@localhost compose_test]$ cat docker-compose.yml
version: 3

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999
```

🌞 Lancez les deux conteneurs

```bash
[lm33@localhost compose_test]$ docker compose up -d
[+] Running 3/3
 ✔ conteneur_nul 1 layers [⣿]      0B/0B      Pulled                                             3.1s
   ✔ bc0734b949dc Already exists                                                                 0.0s
 ✔ conteneur_flopesque Pulled                                                                    3.5s
[+] Running 3/3
 ✔ Network compose_test_default                  Created                                         0.6s
 ✔ Container compose_test-conteneur_nul-1        Sta...                                          0.1s
 ✔ Container compose_test-conteneur_flopesque-1  Started
```

🌞 Vérifier que les deux conteneurs tournent

```bash
[lm33@localhost compose_test]$ docker compose top
compose_test-conteneur_flopesque-1
UID    PID     PPID    C    STIME   TTY   TIME       CMD
root   11394   11354   0    10:28   ?     00:00:00   sleep 9999

compose_test-conteneur_nul-1
UID    PID     PPID    C    STIME   TTY   TIME       CMD
root   11385   11340   0    10:28   ?     00:00:00   sleep 9999
```

🌞 Pop un shell dans le conteneur conteneur_nul

```bash
[lm33@localhost compose_test]$ docker exec -it 6b51003a4dd6 bash
root@6b51003a4dd6:/# apt update
Get:1 http://deb.debian.org/debian bookworm InRelease [151 kB]
...
All packages are up to date.
root@6b51003a4dd6:/# apt-get install iputils-ping -y
Reading package lists... Done
...
Setting up iputils-ping (3:20221126-1) ...
root@6b51003a4dd6:/# ping compose_test-conteneur_flopesque-1
PING compose_test-conteneur_flopesque-1 (172.18.0.3) 56(84) bytes of data.
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.3): icmp_seq=1 ttl=64 time=0.087 ms
...
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.087/0.096/0.105/0.009 ms
```

