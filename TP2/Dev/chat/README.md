# Installation et utilisation de l'image Chat


## 1. Installation

Après avoir clone le repos, vous devez vous rendre dans le dossier ```./chat_build``` :

```bash
cd ./chat_build
```

Puis il faut build l'image Chat avec la commande suivante :

```bash
docker build . -t chat
```

Ici nous avons utilisé le paramètre ```-t``` qui permet de definir le nom de notre image.

vous pouvez y renseigner le nom que vous souhaitez mais il faudra penser à le modifier dans les étapes suivante.


## 2. Utilisation

Après avoir suivi l'installation de l'image Chat nous allons voir comment l'utiliser.

A partir d'ici je vais remplacer le nom de l'image par ```<image>```.

**Pour lancer le conteneur avec le fichier docker-compose :**

Retourner à la racine du dossier chat et executez la commande suivante

```bash
docker compose up
```

Cela lancera 2 images, l'image ```<image>``` qui correspond à notre service de chat et l'image ```redis``` qui est nécessaire à la mise en service de notre service.

**Pour lancer le conteneur avec la commande docker run et les valeurs par défault:**

Lancement du service redis
```bash
docker run -d -p 6379:6379 --restart=always redis
```
Lancement de notre service de chat
```bash
docker run -p 8080:8765 -d <image>
```

**Pour lancer le conteneur avec la commande docker run et des paramètres personnalisés:**

Lancement du service redis
```bash
docker run -d -p 6379:6379 --restart=always redis
```
Lancement de notre service de chat
```bash
docker run -e CHAT_PORT=8000 MAX_USERS=5 -p 8080:8000 -d <image>
```

La variable ```CHAT_PORT``` permet de definir le port sur lequel le service de chat écoutera dans le conteneur.
Port par défault ```8765```.

La variable ```MAX_USERS``` permet de definir le nombre d'utilisateurs pouvant se connecter simultanément au chat.
Nombre par défault ```10```.
