import asyncio
import websockets
import logging
import json
import pickle
import argparse
import redis.asyncio as redis
import sys
from Client import Client
from configparser import ConfigParser
import os

configFile = open('/app/config/server.conf')
config = ConfigParser(allow_no_value=True)
config.read_string(configFile.read())

ClientConnection = {} # WEBSOCKETS : REDIS CLIENT

redisDB = redis.Redis(host="redis", port=6379)

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", action="store", default=int(config["DEFAULT"]["PORT"]), type=int, help="choice a port beetween : 0 and 65535")
parser.add_argument("-a", "--address", action="store", default=config["DEFAULT"]["HOST"], type=str, help="choice server host")
args = parser.parse_args()

configFile.close()

host=args.address
port=args.port
max_users=int(config["DEFAULT"]["MAX_USERS"])

try:
    max_users=int(os.environ["MAX_USERS"])
except:
    logging.warning(f"La variable global MAX_USERS n'est pas renseigné ou n'est pas valide. La valeur par défault '{int(config["DEFAULT"]["MAX_USERS"])}' utilisé")
    max_users=int(config["DEFAULT"]["MAX_USERS"])

try:
    port=int(os.environ["CHAT_PORT"])
except:
    logging.warning(f"La variable global CHAT_PORT n'est pas renseigné ou n'est pas valide. Port par défault '{port}' utilisé")
    port=args.port




if 0 > port or port > 65535:
    print("ERROR Le port spécifié n'est pas un port possible (de 0 à 65535).")
    sys.exit(1)
if 0<= port <= 1024:
    print("ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.")
    sys.exit(2)

logging.basicConfig(level=logging.DEBUG ,filename="/app/log/server.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

async def sendAll(clientsConnection:dict, msg,exception=[]):
    exception = [x for x in exception]
    for connection,red in clientsConnection.items():
        if connection in exception or not red._connection:continue
        await connection.send(msg)




async def handle_client_msg(websocket:websockets.WebSocketServerProtocol):
    global ClientConnection

    if len(ClientConnection.keys()) == int(max_users):
        return

    while True:
        try:
            data = await websocket.recv()
            client = Client(addr=(websocket.remote_address[0],websocket.remote_address[1],),pseudo=data)
            
            if websocket not in ClientConnection.keys():
                ClientConnection[websocket] = None
                logging.info(f"New connection from {client._ip}:{client._port} : {client._pseudo}") 
                if not client.id in await redisDB.keys():
                    await redisDB.set(client.id,pickle.dumps(client))
                    ClientConnection[websocket] = pickle.loads(await redisDB.get(client.id))
                    msg = {
                        "type":"annonce",
                        "author":client._pseudo,
                        "content":"s'est connecté",
                        "time":"time"
                    }
                else:
                    logging.info(f"New connection from {client._ip}:{client._port} : {client._pseudo}")
                    client = pickle.loads(await redisDB.get(client.id))
                    client._connection = True
                    await redisDB.set(client.id,pickle.dumps(client))
                    msg = {
                        "type":"annonce",
                        "author":client._pseudo,
                        "content":"s'est reconnecté",
                        "time":"time"
                    }
                    ClientConnection[websocket] = pickle.loads(await redisDB.get(client.id))
                
                msg = json.dumps(msg)

            else :
                logging.info(f"New message from {client._ip}:{client._port} : {data}") 
                client = pickle.loads(await redisDB.get(client.id))
                msg = json.loads(data)
                msg["author"] = client._pseudo
                msg = json.dumps(msg)
                print(msg)
            await sendAll(ClientConnection,msg, [websocket])
        except websockets.ConnectionClosedOK as ex:
            client = pickle.loads(await redisDB.get(client.id))
            client._connection = False
            await redisDB.set(client.id,pickle.dumps(client))
            msg = {
                "type":"annonce",
                "author":client._pseudo,
                "content":"s'est déconnecté",
                "time":"time"
            }
            msg = json.dumps(msg)
            del ClientConnection[websocket]
            logging.info(f"{client._ip}:{client._port} : {client._pseudo} disconnected")
            await sendAll(ClientConnection,msg)
            print(f"\n\n\n{ex}\n\n\n")
            break
        except Exception as ex:
            print(f"\n\n\n{ex}\n\n\n")



async def main():
    async with websockets.serve(handle_client_msg, host, port):
        logging.info(f"Le serveur tourne sur {host}:{port}")
        await asyncio.Future()
    await redisDB.aclose()

if __name__ == "__main__":
    asyncio.run(main())


