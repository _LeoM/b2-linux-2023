Pour build l'image :

```bash
docker build .\calc_build\ -t calc
```

Pour lancer le conteneur :

```bash
docker compose up

-----------------

docker run -e CALC_PORT=6767 -p 8080:6767 -d calc
```

La variable ```CALC_PORT``` permet de definir le port sur lequel la calculatrice écoutera dans le conteneur.
Port par défault ```9999```.

