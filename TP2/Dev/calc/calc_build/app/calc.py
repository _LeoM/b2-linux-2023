import socket
import argparse
import sys
import re
import logging
import os

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", action="store", default=9999, type=int, help="choice a port beetween : 0 and 65535")
args = parser.parse_args()

host=''
port=args.port

try:
    port=int(os.environ["CALC_PORT"])
except:
    logging.warning("La variable global CALC_PORT n'est pas renseigné ou n'est pas valide. Port par défault '9999' utilisé")
    port=args.port

autoriz_input = '^(-|)\d{1,6}(\+|-|\*)(-|)\d{1,6}$'

SOMEONE_IS_CONNECT = False

logging.basicConfig(level=logging.DEBUG ,filename="/var/log/calc/calc.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())



if 0 > port or port > 65535:
    print("ERROR Le port spécifié n'est pas un port possible (de 0 à 65535).")
    sys.exit(1)
if 0<= port <= 1024:
    print("ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.")
    sys.exit(2)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(60)
s.bind((host,port))

s.listen(1)
logging.info(f"Le serveur tourne sur {host}:{port}")

while not SOMEONE_IS_CONNECT:
    try:
        conn, addr = s.accept()
        SOMEONE_IS_CONNECT = True
    except:
        logging.warning("Aucun client depuis plus de une minute.")

logging.info(f"Un client {addr[0]} s'est connecté.")

while True:
    try:
        data=conn.recv(1024)

        if not data: break

        message = data.decode()
        logging.info(f"Le client {addr[0]} a envoyé {message}")

        if not (re.match(autoriz_input, message)): 
            logging.error(f"Le message reçu : {message} n'est pas valide")
            raise ValueError("Input not valide")

    except socket.error as err:
        print(f"Error Occured.\n{err}")
        break
    
    reponse = str(eval(message))

    conn.sendall(reponse.encode())
    logging.info(f'Réponse envoyée au client {addr[0]} : {reponse}.')


conn.close()