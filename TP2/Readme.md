# TP2 : Utilisation courante de Docker

## I. Commun à tous : Stack PHP

### I. Packaging de l'app PHP

[🌞 docker-compose.yml](./Commun/docker-compose.yml)


!!!!!!!!!!!!!!!!!!!!!!    REVOIR INJECTION D'UN FICHIER SQL


## II. Packaging et environnement de dév local

### I. Packaging

1. Calculatrice

🌞 Packager l'application de calculatrice réseau

[Dockerfile](./Dev/calc/calc_build/Dockerfile)

[docker-compose](./Dev/calc/docker-compose.yml)

[README.md](./Dev/calc/README.md)

[calc.py](./Dev/calc/calc_build/app/calc.py)


🌞 Environnement : adapter le code si besoin

```python
try:
    port=int(os.environ["CALC_PORT"])
except:
    logging.warning("La variable global CALC_PORT n'est pas renseigné ou n'est pas valide. Port par défault '9999' utilisé")
    port=args.port
```


🌞 Logs : adapter le code si besoin

```powershell
PS C:\Users\macel> docker logs calc-calculatrice-1
Le serveur tourne sur :9999
Aucun client depuis plus de une minute.
Un client 172.18.0.1 s'est connecté.
Le client 172.18.0.1 a envoyé 1+1
Réponse envoyée au client 172.18.0.1 : 2.
```

2. Chat room

🌞 Packager l'application de chat room

[Dockerfile](./Dev/chat/chat_build/Dockerfile)

[docker-compose](./Dev/chat/docker-compose.yml)

[README.md](./Dev/chat/README.md)

[chat.py](./Dev/chat/chat_build/app/chat.py)